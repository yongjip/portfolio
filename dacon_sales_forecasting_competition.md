# Sales Forecasting Competition on Dacon.io (Sept 2018) 


The goal of the competition was to predict future sales in 200 small businesses. There were 70 people participating in the competition. The participants used various techniques like representation learning, XGBoost, deep learning, etc. I chose to use a traditional time-series model, ARIMA and preprocessed the data using basic statistics and the log transformation. I won the competition and received a cash prize of $5,000 and 100,000 ZPR coins.

![alt text1][dacon_ranking]

[dacon_ranking]: images/daton_ranking.png

[https://dacon.io/rank1](https://dacon.io/rank1)


## Description of the Competition
For SMB loans, the traditional banks have carried out a credit-scoring or collateral-based reviewing processes, both based upon an old scoring system of FICO. However, a South Korean Fintech corporation, FUNDA, tries to provide financial opportunity for a myriad of SMB owners, who neither have high FICO scores nor eligible collaterals, by accurately analyzing their businesses and forecasting the future sales during the loan period.
In this opening competition, you will establish a ‘new scoring model’ that predicts the future sales in an anonymous business differently but innovatively during its loan-and-repayment period (*FUNDA sets a loan limit based upon the sales predicted). The more accurately your model works, the more ordinary SMB owners will get to have better loan limits with lower interest rates.

*The mission is to predict the total sales for 100 days from the next day of last given transaction for each store.*

A hypothetical SMB owner(s) will receive a `virtual loan(s)` for 100 days, as the same amount with the value you forecast in the given business. In this simulation, if actual sales are less than the forecasted, a financial loss occurs over the principals; if the sales surpass the forecasted, on the other hand, profits will return at an annual interest rate of 13%. To estimate the simulation result(s) more accurately and briefly, your submission(s) will be evaluated with the following formula:

```math
\displaystyle\sum_{i=1}^n
\begin{cases}
    \hat{y_{i}} * 13\% * 100 / 365 
        &\text{if } (y_{i} - \hat{y_{i}}) \ge 0\\
   y_{i} - \hat{y_{i}}
        &\text{if } (y_{i} - \hat{y_{i}}) \lt 0
\end{cases}
```

* Here, $`y_i`$ represents ‘the sales of a single store’ during the loan period and $`\hat{y_{i}}`$ represents the corresponding prediction.
* Please be noted that the data is sampled by the `card ID` and the unit of amount column is not KRW(Korean currency).

[Data fields]

* store_id - a unique id for each store, unique only in the same file. (stores may have been opened or closed in the middle)
* date - transacted date
* time - transacted time
* card_id - a unique hashed card number
* amount – amount(s) of sales / negative numbers ($`<0`$) for all the canceled transactions.
* installments - installments period in the month.
* days_of_week - 0 for Monday, 6 for Sunday
* holiday - 1 for the public holiday, 0 for otherwise.

## Preprocessing

There are negative values in the data set, which represent canceled transactions. These negative values increase variance, and thus prediction interval. Transactions with negative values were removed with corresponding positive transactions that had the same card_id and had a date less than or equal to the negative transaction’s date.

Each business’ transaction data were aggregated by 28, 14, or 7 days based on the availability of data. These aggregation levels are selected because they reduce variance by removing seasonal variations due to day of the week without hurting yearly seasonal pattern $`(365 / 7 = 52.14; 365 / 14 = 26.07; 365 / 28 = 13.04)`$.

I used six as the minimum time-buckets. For example, if a business has only five periods after transformed to 28-day aggregates, then its data are converted to 14-day aggregates. The process creates ten aggregated observations. I chose to use six as the minimum, because it slightly outperformed other numbers, such as four, eight, and twelve, in prediction accuracy.

In the case that the selected aggregation level is not a multiplier of daily aggregates of available records, the data from the beginning of the series were discarded, as the letter observations are considered as more relevant.

Log of aggregated sales amount was used instead of sales amount because it is more normally distributed. If there are null (infinity) values after taking the log, then the probability of no sales was calculated based on the count of null values (see below). The probability of no sales was used to calculate final forecasts.

Probability of no sales = number of null observations / total number of observations

Final forecasts = (1 - probability of no sales) * forecasts

## Scoring Model

```math
\text{Return} = \displaystyle\sum_{i=1}^n
\begin{cases}
    \hat{y_{i}} * 13\% * 100 / 365 
        &\text{if } (y_{i} - \hat{y_{i}}) \ge 0\\
   y_{i} - \hat{y_{i}}
        &\text{if } (y_{i} - \hat{y_{i}}) \lt 0
\end{cases}
```

The formula above implies that marginal profit percent from additional investment equals $`0.13 * 100 / 365`$ when the sales amount is greater than forecasted, and marginal loss percent equals 1 when the sales amount is less than forecasts. The optimal probability of sales greater than forecasts that maximizes expected return is where marginal loss equals marginal profit. Expected return, optimal $`p`$, $`p^{*}`$, and optimal $`z`$ score, $`z\text{-score}^{*}`$, are as follows:

```math
E(\text{return}) = \alpha p - \beta (1-p)\\

p^{*} = \beta / \alpha - 1\\

z\text{-score}^{*} = 1.82 \text{ (assuming normal distribution)}\\
```

$`p`$ = probability that actual sales are greater than sales forecasts

$`\alpha = 0.13 * 100 / 365`$ (marginal profit percent from additional investment when sales > forecasts)

$`\beta = 1`$ (marginal loss percent from additional investment when sales < forecasts)

$`p^{*}`$ is 0.9656. In other words, to maximize the expected return the actual sales must greater than the forecasted at probability of 0.9656. Assuming normal distribution, $`z\text{-score}^{*}`$ is 1.82.

Using ARIMA model with optimal pqr selected, between 0 and 1, based on AIC (Akaike Information Criterion), I got LN sales forecasts and standard deviation, $`\sigma`$, then calculated adjusted LN sales forecasts to maximize expected return as follows: 
```math
\text{LN sales forecasts}_{i} = \mu_{i} - \sigma_{i} * z\text{-score}^{*}
```

I converted the log of adjusted sales forecasts to sales amount using the exponential function:
```math
\text{sales forecasts}_{i} = e^{\text{LN sales forecasts}_{i}}
```

```python

import pandas as pd
import numpy as np
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.stattools import adfuller, kpss
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
import math
import seaborn as sns
import warnings
import statsmodels.api as sm
import matplotlib.pyplot as plt
import itertools
import scipy.stats as st
from datetime import datetime, timedelta
warnings.filterwarnings("ignore")  # specify to ignore warning messages

sns.set(color_codes=True)

test = pd.read_csv('data/test.csv')
submission = pd.read_csv('data/submission.csv')


df_copy = test.copy()
df_copy.date = pd.to_datetime(df_copy.date)

# df_copy.date = pd.to_datetime(df_copy.date + " " + df_copy.time, format='%d/%m/%y %H:%M:%S')

df_copy.date = pd.to_datetime(df_copy.date.astype(str) + " " + df_copy.time, format='%Y-%m-%d %H:%M:%S')

# Remove negative values from the data set.


def reduce_noise_by_removing_neg_vals(df_copy):
    df_pos = df_copy[df_copy.amount > 0]
    df_neg = df_copy[df_copy.amount < 0]

    # exact_match = []
    # larger_match = []
    # no_match = []

    start = datetime.now()

    for nega_i in df_neg.to_records()[:]:
        store_i = nega_i[1]
        date_i = nega_i[2]
        card_i = nega_i[4]
        amt_i = nega_i[5]
        row_i = df_pos[df_pos.store_id == store_i]
        row_i = row_i[row_i.card_id == card_i]
        row_i = row_i[row_i.amount >= abs(amt_i)]
        row_i = row_i[row_i.date <= date_i]
        if len(row_i[row_i.amount == abs(amt_i)]) > 0:
            row_i = row_i[row_i.amount == abs(amt_i)]
            matched_row = row_i[row_i.date == max(row_i.date)]
            # df_pos.loc[matched_row.index, 'amount'] = 0
            df_pos = df_pos.loc[~df_pos.index.isin(matched_row.index), :]
        elif len(row_i[row_i.amount > abs(amt_i)]) > 0:
            matched_row = row_i[row_i.date == max(row_i.date)]
            df_pos.loc[matched_row.index, 'amount'] = matched_row.amount + amt_i
        # else:
        #     pass
            # no_match.append(nega_i)
    end = datetime.now()
    time_took = (end - start).seconds / 60

    print(round(time_took, 2))
    return df_pos

df_pos = reduce_noise_by_removing_neg_vals(df_copy)


def adf_test(y):
    # perform Augmented Dickey Fuller test
    print('Results of Augmented Dickey-Fuller test:')
    dftest = adfuller(y, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['test statistic', 'p-value', '# of lags', '# of observations'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value ({})'.format(key)] = value
    print(dfoutput)


def ts_diagnostics(y, lags=None, title='', filename=''):
    '''
    Calculate acf, pacf, qq plot and Augmented Dickey Fuller test for a given time series
    '''
    if not isinstance(y, pd.Series):
        y = pd.Series(y)

    # weekly moving averages (5 day window because of workdays)
    rolling_mean = pd.Series.rolling(y, window=2).mean()
    rolling_std = pd.Series.rolling(y, window=2).std()

    fig = plt.figure(figsize=(14, 12))
    layout = (3, 2)
    ts_ax = plt.subplot2grid(layout, (0, 0), colspan=2)
    acf_ax = plt.subplot2grid(layout, (1, 0))
    pacf_ax = plt.subplot2grid(layout, (1, 1))
    qq_ax = plt.subplot2grid(layout, (2, 0))
    hist_ax = plt.subplot2grid(layout, (2, 1))

    # time series plot
    y.plot(ax=ts_ax)
    rolling_mean.plot(ax=ts_ax, color='crimson')
    rolling_std.plot(ax=ts_ax, color='darkslateblue')
    plt.legend(loc='best')
    ts_ax.set_title(title, fontsize=24)

    # acf and pacf
    plot_acf(y, lags=lags, ax=acf_ax, alpha=0.5)
    plot_pacf(y, lags=lags, ax=pacf_ax, alpha=0.5)

    # qq plot
    sm.qqplot(y, line='s', ax=qq_ax)
    qq_ax.set_title('QQ Plot')

    # hist plot
    y.plot(ax=hist_ax, kind='hist', bins=25)
    hist_ax.set_title('Histogram')
    plt.tight_layout()
    plt.show()

    # perform Augmented Dickey Fuller test
    print('Results of Dickey-Fuller test:')
    dftest = adfuller(y, autolag='AIC')
    dfoutput = pd.Series(dftest[0:4], index=['test statistic', 'p-value', '# of lags', '# of observations'])
    for key, value in dftest[4].items():
        dfoutput['Critical Value (%s)' % key] = value
    print(dfoutput)
    return


df = df_pos.copy()
test_groupby_date_store = df.groupby(['date', 'store_id'])['amount', 'holyday'].sum()
test_groupby_date_store = test_groupby_date_store.reset_index()

test_groupby_date_store = test_groupby_date_store.set_index('date')
store_list = test_groupby_date_store.store_id.unique()

store_list.sort()


def get_optimal_params(y):
    # Define the p, d and q parameters to take any value between 0 and 1

    param_dict = {}
    for param in pdq:
        try:
            mod = sm.tsa.statespace.SARIMAX(y,
                                            order=param,
                                            )
            results = mod.fit()
            model = ARIMA(y, order=param)
            results_ARIMA = model.fit(disp=-1)
            results_ARIMA.summary()
            param_dict[results.aic] = param
        except:
            continue

    min_aic = min(param_dict.keys())
    optimal_params = param_dict[min_aic]
    return optimal_params


# 49.83673 (28-21-14-7 model)
# sampling_p = 28
# mean_period = 2 * 3 #14 * 2*3
#
# predic_len = math.floor(100 / sampling_p)
# expected_return_pct_lending = 0.13 * (100 + 16 + 7) / 365 == 54.62457
#expected_return_pct_lending = 0.13 * (100 + 16 + 6.8) / 365 == 49.80176
# expected_return_pct_lending = 0.13 * (100 + 16 + 6) / 365 == 49.83673
# expected_loss_pct_lending = 1.00
# min_period = 6


sampling_p = 28
mean_period = 2 * 3 #14 * 2*3

predic_len = math.floor(100 / sampling_p)

expected_return_pct_lending = 0.13 * (100 + 16 + 6.8) / 365
expected_loss_pct_lending = 1.00
optimal_prob = expected_loss_pct_lending / (expected_loss_pct_lending + expected_return_pct_lending)
optimal_z_score = st.norm.ppf(optimal_prob)

min_period = 6


max_pdq = 2
p = d = q = range(0, max_pdq)
pdq = list(itertools.product(p, d, q))


pdqs = dict()
print(optimal_prob)
print(optimal_z_score)
output_file_name_fmt = 'outputs/py_4arima_pos_sep_{optimal_p}-{sampling_period}_no_sales_prob&no mean{mean_period}&min_period {min_period}_pdq{max_pdq}.csv'
output_file_name = output_file_name_fmt.format(optimal_p=round(optimal_prob, 4),
                                               sampling_period=sampling_p,
                                               mean_period=mean_period,
                                               min_period=min_period,
                                               max_pdq=max_pdq)

submission_copy = submission.copy()


def arima_main(input_df, sampling_period_days, fcst_period):
    input_df = input_df[len(input_df) % sampling_period_days:].resample(str(sampling_period_days) + 'D').sum()
    prob_of_no_sales = len(input_df[(input_df.amount == 0) | (input_df.amount.isna())]) / len(input_df)
    ts_log = np.log(input_df.amount)
    ts_log = ts_log[~ts_log.isin([np.nan, np.inf, -np.inf])]

    if len(ts_log) < min_period:
        return None
    if sampling_period_days >= 28:
        expected_return_pct_lending = 0.13 * (100 + 16 + 6.8) / 365
    elif sampling_period_days >= 14:
        expected_return_pct_lending = 0.13 * (100 + 16 + 14) / 365
    else:
        expected_return_pct_lending = 0.13 * (100 + 16 + 6.8) / 365

    expected_loss_pct_lending = 1.00
    optimal_prob = expected_loss_pct_lending / (expected_loss_pct_lending + expected_return_pct_lending)
    optimal_z_score = st.norm.ppf(optimal_prob)

    optimal_params = get_optimal_params(ts_log)
    pdqs[store_i] = optimal_params

    model = ARIMA(ts_log, order=optimal_params)
    results_ARIMA = model.fit(disp=-1)
    fcst = results_ARIMA.forecast(fcst_period)

    fcst_means = fcst[0]
    fcst_stds = fcst[1]
    fcst_i = fcst_means - (fcst_stds * optimal_z_score)
    fcst_i = sum(map(lambda x: np.exp(x) if np.exp(x) > 0 else 0, fcst_i))
    prediction_i = fcst_i * (1 - prob_of_no_sales)
    return prediction_i


for store_i in store_list[:]:
    prediction_i = None
    test_df = test_groupby_date_store[test_groupby_date_store.store_id == store_i]
    test_df_daily = test_df.resample('D').sum()
    prediction_i = arima_main(test_df_daily, sampling_period_days=28, fcst_period=3)
    # if prediction_i is None:
    #     prediction_i = arima_main(test_df_daily, sampling_period_days=21, fcst_period=4)
    if prediction_i is None:
        prediction_i = arima_main(test_df_daily, sampling_period_days=14, fcst_period=7)
    if prediction_i is None:
        prediction_i = arima_main(test_df_daily, sampling_period_days=7, fcst_period=12)
    if prediction_i is None:
        test_df = test_df_daily[len(test_df_daily) % 14:].resample('14D').sum()

        prob_of_no_sales = len(test_df[(test_df.amount == 0) | (test_df.amount.isna())]) / len(test_df)
        ts_log = ts_log[~ts_log.isin([np.nan, np.inf, -np.inf])]
        ts_log_wkly = np.log(test_df.amount)

        estimated_amt = np.exp(ts_log_wkly.mean() - ts_log_wkly.std() * optimal_z_score) * (1 - prob_of_no_sales)
        prediction_i = estimated_amt * mean_period

    submission_copy.loc[submission_copy['store_id'] == store_i, 'total_sales'] = prediction_i

submission_copy.to_csv(output_file_name, index=False)

print(output_file_name)

```
